const express = require("express");
const bodyParse = require("body-parser");
const jwt = require("jsonwebtoken");
const path = require("path");
const { Sequelize, DataTypes } = require("sequelize");
const { request } = require("http");
const app = express();
const session = require("express-session");
// const flash = require('express-flash')
const { port = 3000 } = process.env;
app.use(express.urlencoded());

app.use(
  session({
    secret: "SECRET",
    resave: false,
    saveUninitialized: false,
  })
);

const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());
// app.use(flash())
app.set("view engine", "ejs");

const router = require("./router");

app.use(express.static(path.join(__dirname, "assets")));
app.use(bodyParse.json());
app.use(express.json());

const sequelize = new Sequelize("chapter_6", "postgres", "ryushikimura", {
  host: "localhost",
  dialect: "postgres",
  port: 8080,
});

async function testConnection() {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}

app.use(router);
const { send } = require("process");
const { render } = require("ejs");
testConnection();

app.get("/datauser", (req, res) => {
  res.status(200).json(posts);
});

app.listen(port, () => console.log(`example app listening at ${port}`));
