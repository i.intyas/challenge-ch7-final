const { Players, user_game_biodata, history } = require("../models");
let posts = require("../data/users.json");
const passport = require("../lib/passport");
const session = require("express-session");

module.exports = {
  signupSubmit: (req, res, next) => {
    // Kita panggil static method register yang sudah kita buat tadi
    Players.register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },

  create: (req, res) => {
    res.render("dashboard/create.ejs");
  },

  createSubmit: async (req, res) => {
    user = await Players.create(req.body);
    req.body.userId = user.id;
    await history.create(req.body);
    await user_game_biodata.create(req.body);
    res.redirect("../list");
  },

  list: async (req, res) => {
    const player = await Players.findAll({
      include: [{ model: user_game_biodata, as: "user_game_biodata" }],
    });
    res.render("dashboard/list.ejs", {
      player: player,
    });
  },

  biodata: async (req, res) => {
    const player = await Players.findAll({
      include: [{ model: user_game_biodata, as: "user_game_biodata" }],
    });
    res.render("dashboard/biodata.ejs", {
      player: player,
    });
  },

  history: async (req, res) => {
    const player = await Players.findAll({
      include: [{ model: history, as: "history" }],
    });
    res.render("dashboard/history.ejs", {
      player: player,
    });
  },

  delete: async (req, res) => {
    await history.destroy({
      where: {
        userId: req.params.id,
      },
    });
    await user_game_biodata.destroy({
      where: {
        UserId: req.params.id,
      },
    });
    await Players.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.redirect("../list");
  },

  edit: async (req, res) => {
    const player = await Players.findOne({
      where: {
        id: req.params.id,
      },
    });
    const player2 = await user_game_biodata.findOne({
      where: {
        UserId: req.params.id,
      },
    });
    const player3 = await history.findOne({
      where: {
        userId: req.params.id,
      },
    });
    res.render("dashboard/edit.ejs", {
      player: player,
      player2: player2,
      player3: player3,
    });
  },

  editSubmit: async (req, res) => {
    await Players.update(req.body, { where: { id: req.params.id } });
    await user_game_biodata.update(req.body, {
      where: { UserId: req.params.id },
    });
    await history.update(req.body, { where: { userId: req.params.id } });
    res.redirect("../../list");
  },

  // checkLogin: passport.authenticate('local', {
  //     successRedirect: '/',
  //     failureRedirect: '/login',
  //     failureFlash: true // Untuk mengaktifkan express flash
  // }),
};
