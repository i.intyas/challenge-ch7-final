const {
  Users,
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
} = require("../models");
const passport = require("../lib/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const { param } = require("../router");

module.exports = {
  room: async (req, res) => {
    const payload = req.body;
    function makecode(length) {
      var result = "";
      var characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      return result;
    }

    const code = makecode(4);
    console.log(req.user);
    const savePayload = {
      code: code,
      roundNum: payload.roundNum,
      createdBy: req.user.id,
      status: "ACTIVE",
      winnerId: 1,
    };
    const saveToDb = await Rooms.create(savePayload);
    res.send(saveToDb);
  },
};
