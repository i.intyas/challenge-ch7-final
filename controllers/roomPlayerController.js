const {
  Users,
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
} = require("../models");
const passport = require("../lib/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const { param } = require("../router");

module.exports = {
  joinRoom: async (req, res) => {
    const payload = req.body;
    const infoRoom = await Rooms.findOne({
      where: {
        code: payload.code,
      },
    });
    if (!infoRoom) {
      res.send("kode ruangan salah!");
    } else {
      const savePayLoad = {
        roomId: infoRoom.id,
        playerId: req.user.id,
      };
      console.log("root path", req.user);
      const howmanyPLayers = await RoomPlayers.findAll({
        where: {
          roomId: savePayLoad.roomId,
        },
      });
      if (howmanyPLayers.length < 2) {
        const infoRoomPlayers = await RoomPlayers.findOne({
          where: {
            roomId: savePayLoad.roomId,
            playerId: req.user.id,
          },
        });
        if (!infoRoomPlayers) {
          const createRoom = await RoomPlayers.create(savePayLoad);
          res.send("player sudah masuk");
        } else {
          res.send("player tidak boleh join lebih dari satu kali");
        }
      } else {
        res.send("ruangan sudah penuh!");
      }
    }
  },
};
