const {
  Users,
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
} = require("../models");
const passport = require("../lib/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const { param } = require("../router");

module.exports = {
  registrasi: async (req, res) => {
    const createUser = await Users.register(req.body);
    res.send({
      id: createUser.id,
      email: createUser.email,
      username: createUser.username,
      name: createUser.name,
    });
  },
};
