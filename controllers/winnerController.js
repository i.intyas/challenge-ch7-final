const {
  Users,
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
} = require("../models");
const passport = require("../lib/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const { param } = require("../router");

module.exports = {
  winner: async (req, res) => {
    const theWinner = await Rooms.findOne({
      where: {
        code: "810",
      },
    });

    if (theWinner.winnerId === null) {
      res.send("hasil pertandingan seri atau belum ada pemenangnya");
    } else {
      res.send(theWinner);
    }
  },
};
