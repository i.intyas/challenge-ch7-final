const {
  Users,
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
} = require("../models");
const passport = require("../lib/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const { param } = require("../router");

module.exports = {
  play: async (req, res) => {
    const payload = req.body;
    const infoRoom = await Rooms.findOne({
      where: {
        code: payload.code,
      },
    });

    //di bawah ini untnuk mengecek apakah kode ruangan sudah benar
    if (!infoRoom) {
      res.send("kode ruangan salah!");
    } else {
      const savePayload = {
        code: infoRoom.code,
        roundNum: infoRoom.roundNum,
        roomId: infoRoom.id,
        status: "ACTIVE",
        playerId: req.user.id,
        selected: payload.selected,
      };

      async function ultimateWinner() {
        const playerIdentity = await RoomPlayers.findAll({
          where: {
            roomId: savePayload.roomId,
          },
        });

        const player1 = await GameRounds.findAll({
          where: {
            winnerId: playerIdentity[0].playerId,
            roomId: savePayload.roomId,
          },
        });
        const player2 = await GameRounds.findAll({
          where: {
            winnerId: playerIdentity[1].playerId,
            roomId: savePayload.roomId,
          },
        });
        player1Score = player1.length;
        player2Score = player2.length;

        if (player1Score > player2Score) {
          const ultWinner = {
            winnerId: playerIdentity[0].playerId,
            status: "FINISHED",
          };

          await Rooms.update(ultWinner, {
            where: {
              id: savePayload.roomId,
            },
          });
          res.send(
            `Permainan Selesai,pemenang akhir adalah player dengan Id ${ultWinner.winnerId}, silakan mulai Game Baru di ruangan yang berbeda`
          );
        } else if (player1Score < player2Score) {
          const ultWinner = {
            winnerId: playerIdentity[1].playerId,
            status: "FINISHED",
          };
          console.log("isi ultWinner", ultWinner);
          await Rooms.update(ultWinner, {
            where: {
              id: savePayload.roomId,
            },
          });
          res.send(
            `Permainan Selesai,pemenang akhir adalah player dengan Id ${ultWinner.winnerId}, silakan mulai Game Baru di ruangan yang berbeda`
          );
        } else {
          const ultWinner = {
            winnerId: null,
            status: "FINISHED",
          };
          console.log("isi ultWinner", ultWinner);
          await Rooms.update(ultWinner, {
            where: {
              id: savePayload.roomId,
            },
          });

          console.log("belum ada ultimate winner!");
          res.send(
            "Permainan Selesai,hasil pertandingan seri, silakan mulai Game Baru di ruangan yang berbeda"
          );
        }
      }

      async function startGame() {
        //di bawah ini untunk memastikan bahwa hanya 2 pemain yang bisa kasih choice => dengan mengecek apakah Id player sama dengan yang di roomplayers
        const isPlayerIdMatched = howmanyPLayers.find(
          (user) => user.playerId === savePayload.playerId
        ); // ini untuk memastikan bahwa yang ikut main  hanya player yang ada di room game itu saja
        if (!isPlayerIdMatched) {
          res.send("Id player tidak ditemukan di Room ini"); // ini untuk kasus player yang tidak termasuk di game room
        } else {
          //ini untuk mendapatkan info roundId
          const infoRound = await GameRounds.findOne({
            where: {
              roomId: savePayload.roomId,
              status: "ACTIVE",
            },
          });
          savePayload.roundId = infoRound.id;

          //ini untuk mengecek apakah player sudah pernah memasukkan pilihan
          const isChoiceMade = await GameRoundHistories.findOne({
            where: {
              playerId: savePayload.playerId,
              roundId: savePayload.roundId,
            },
          });
          //di bawah adalah untuk kasus belum ada pilihan yang dimasukkan oleh player
          if (!isChoiceMade) {
            const updateGamRound = await GameRounds.update(savePayload, {
              where: {
                id: savePayload.roundId,
              },
            });
            const updateGameHistory = await GameRoundHistories.create(
              savePayload
            );

            //ini untuk menentukan pemenang
            const playerChoices = await GameRoundHistories.findAll({
              where: {
                roundId: savePayload.roundId,
              },
            });
            //kalau di gamehistory sudah ada 2 pilihan player, saatnya menentukan pemenang per ronde
            if (playerChoices.length === 2) {
              let selected1 = playerChoices[0].selected;
              let selected2 = playerChoices[1].selected;

              if (
                (selected1 === "ROCK" && selected2 === "PAPER") ||
                (selected1 === "SCISSORS" && selected2 === "ROCK") ||
                (selected1 === "PAPER" && selected2 === "SCISSORS")
              ) {
                savePayload.winnerId = playerChoices[1].playerId;
                console.log(
                  `pemenang ronde ini = player ${savePayload.winnerId}`
                );
                updateWinner();
              } else if (
                (selected1 === "ROCK" && selected2 === "SCISSORS") ||
                (selected1 === "PAPER" && selected2 === "ROCK") ||
                (selected1 === "SCISSORS" && selected2 === "PAPER")
              ) {
                savePayload.winnerId = playerChoices[0].playerId;
                console.log(
                  `pemenang ronde ini= player ${savePayload.winnerId}`
                );
                updateWinner();
              } else {
                savePayload.winnerId = null;
                console.log("pemenang ronde ini = seri");
                updateWinner();
              }
            } else {
              res.send("belum ada pemenang");
            }

            async function updateWinner() {
              savePayload.status = "INACTIVE";
              const updateWinnerandStatus = await GameRounds.update(
                savePayload,
                {
                  where: {
                    roomId: savePayload.roomId,
                    status: "ACTIVE",
                  },
                }
              );
              startCalculate();
            }

            async function startCalculate() {
              const isInactive = await GameRounds.findAll({
                where: {
                  roomId: savePayload.roomId,
                  status: "INACTIVE",
                },
              });

              if (isInactive.length >= savePayload.roundNum) {
                ultimateWinner();
              } else {
                res.send("permainan belum selesai, pergi ke ronde selanjutnya");
              }
            }
            // di bawah ini respon untuk kasus player sudah pernah memilih senjata sebelumnya
          } else {
            res.send(
              `pemain ${req.user.username} hanya boleh memilih 1 kali setiap ronde`
            );
          }
        }
      }
      //di bawah ini untk memastikan bahwa di dalam room  2 player yang berbeda dengan room Id yang sama agar permainan bisa dimulai
      const howmanyPLayers = await RoomPlayers.findAll({
        where: {
          roomId: savePayload.roomId,
        },
      });
      if (howmanyPLayers.length === 2) {
        // di bawah ini untuk memastikan apakah ada gameround yang active atau tidak (roomId harus sama). kalau ada, dicek sudah berapa round.
        const infoRound = await GameRounds.findAll({
          where: {
            roomId: savePayload.roomId,
          },
        });

        const isActive = await GameRounds.findOne({
          where: {
            roomId: savePayload.roomId,
            status: "ACTIVE",
          },
        });

        if (infoRound.length <= savePayload.roundNum && isActive) {
          startGame();
        } else if (infoRound.length >= savePayload.roundNum && !isActive) {
          res.send("room in sudah kadaluwarsa, silakan buat Room baru!");
        } else {
          await GameRounds.create(savePayload);
          startGame();
        }
      } else {
        res.send("jumlah pemain harus 2 orang!");
      }
    }
  },
};
