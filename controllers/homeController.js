module.exports = {
  landing: (req, res) => {
    res.render("landing/index.ejs");
  },

  game: (req, res) => {
    res.render("game/index.ejs");
  },

  loginPage: (req, res) => {
    res.render("articles/index.ejs");
  },

  signup: (req, res) => {
    res.render("register/register.ejs");
  },
};
