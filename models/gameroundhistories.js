'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRoundHistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.GameRoundHistories.belongsTo(models.Users,{
        foreignKey : "playerId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })

      models.GameRoundHistories.belongsTo(models.GameRounds,{
        foreignKey:"roundId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })
    }
  }
  GameRoundHistories.init({
    roundId: DataTypes.INTEGER,
    playerId: DataTypes.INTEGER,
    selected: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'GameRoundHistories',
  });
  return GameRoundHistories;
};