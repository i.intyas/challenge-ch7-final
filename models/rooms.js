'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Rooms.hasMany(models.GameRounds,{
        foreignKey : "roomId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })

      models.Rooms.belongsTo(models.Users,{
        foreignKey : "createdBy",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })

      models.Rooms.belongsTo(models.Users,{
        foreignKey : "winnerId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })
    }
  }
  Rooms.init({
    code: DataTypes.STRING,
    roundNum: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    winnerId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'Rooms',
  });
  return Rooms;
};