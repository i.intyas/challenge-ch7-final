'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRounds extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.GameRounds.hasMany(models.GameRoundHistories, {
        foreignKey : "roundId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),
      models.GameRounds.belongsTo(models.Rooms, {
        foreignKey: "roomId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),

      models.GameRounds.belongsTo(models.Users, {
        foreignKey: "winnerId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })
    }
  }
  GameRounds.init({
    roomId: DataTypes.INTEGER,
    winnerId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'GameRounds',
  });
  return GameRounds;
};