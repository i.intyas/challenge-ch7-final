'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomPlayers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.RoomPlayers.belongsTo(models.Users,{
        foreignKey : "playerId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),

      models.RoomPlayers.belongsTo(models.Rooms,{
        foreignKey : "roomId",
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })
    }
  }
  RoomPlayers.init({
    roomId: DataTypes.INTEGER,
    playerId: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'RoomPlayers',
  });
  return RoomPlayers;
};