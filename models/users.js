'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      models.Users.hasMany(models.RoomPlayers, {
        foreignKey : 'playerId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),

      models.Users.hasMany(models.Rooms, {
        foreignKey : 'winnerId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),

      models.Users.hasMany(models.GameRoundHistories, {
        foreignKey : 'playerId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })

    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    
    static register = (payload) => {
      console.log(payload)
      const encryptedPassword = this.#encrypt(payload.password)
     return this.create({
       email: payload.email,
       username : payload.username,
       name : payload.name,
       password : encryptedPassword,
       role : payload.role
      })
    }
    
    /* Method Authenticate and token, untuk login */
    static authenticate = async (payload) => {
      // console.log("payload2",payload.password)
      const user = await Users.findOne({ where: { username : payload.username} }) 
        if (user) {
          // console.log("ini input password", payload.password)
          // console.log("ini user encrypted password di db", user.password)
          const isPasswordValid = bcrypt.compareSync(payload.password,user.password)
          // console.log("isi isPasswordValid ",isPasswordValid)
          if (isPasswordValid){
            const payload = {
              id: user.id,
              username : user.username,
              email: user.email
            }
            // console.log("ini is payload token",payload)
            const secret = 'secret'
            const token = jwt.sign(payload, secret)
            // console.log("ini tokennya",token)
            user.token = token
            console.log("username dan password cocok")
            // console.log("ini isi user", user)
            return user
          } else{
            console.log("password salah")
            return !user
          }
        }else{
          console.log("username tidak ditemukan")
          return !user
        }      
    }
    
    
  }
  Users.init({
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};