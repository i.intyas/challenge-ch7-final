const router = require("express").Router();
const passport = require("./lib/passport");
const registration = require("./controllers/registrationController");
const auth = require("./controllers/authController");
const playing = require("./controllers/playController");
const winning = require("./controllers/winnerController");
const roomPlaying = require("./controllers/roomPlayerController");
const rooming = require("./controllers/roomController");

router.post("/register", registration.registrasi);
router.post("/login", auth.isAuthenticated);
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    console.log("root path", req.user);
    res.send(`selamat datang ${req.user.username}!`);
  }
);
router.post(
  "/room",
  passport.authenticate("jwt", { session: false }),
  rooming.room
);
router.post(
  "/join",
  passport.authenticate("jwt", { session: false }),
  roomPlaying.joinRoom
);
router.post(
  "/play",
  passport.authenticate("jwt", { session: false }),
  playing.play
);
router.get(
  "/winner/810",
  passport.authenticate("jwt", { session: false }),
  winning.winner
);

module.exports = router;
