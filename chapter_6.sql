-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "GameRoundHistories_id_seq";

-- Table Definition
CREATE TABLE "public"."GameRoundHistories" (
    "id" int4 NOT NULL DEFAULT nextval('"GameRoundHistories_id_seq"'::regclass),
    "roundId" int4 NOT NULL,
    "playerId" int4 NOT NULL,
    "selected" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "GameRoundHistories_roundId_fkey" FOREIGN KEY ("roundId") REFERENCES "public"."GameRounds"("id"),
    CONSTRAINT "GameRoundHistories_playerId_fkey" FOREIGN KEY ("playerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "GameRounds_id_seq";

-- Table Definition
CREATE TABLE "public"."GameRounds" (
    "id" int4 NOT NULL DEFAULT nextval('"GameRounds_id_seq"'::regclass),
    "roomId" int4 NOT NULL,
    "winnerId" int4,
    "status" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "GameRounds_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "public"."Rooms"("id"),
    CONSTRAINT "GameRounds_winnerId_fkey" FOREIGN KEY ("winnerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "RoomPlayers_id_seq";

-- Table Definition
CREATE TABLE "public"."RoomPlayers" (
    "id" int4 NOT NULL DEFAULT nextval('"RoomPlayers_id_seq"'::regclass),
    "roomId" int4 NOT NULL,
    "playerId" int4 NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "RoomPlayers_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "public"."Rooms"("id"),
    CONSTRAINT "RoomPlayers_playerId_fkey" FOREIGN KEY ("playerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "Rooms_id_seq";

-- Table Definition
CREATE TABLE "public"."Rooms" (
    "id" int4 NOT NULL DEFAULT nextval('"Rooms_id_seq"'::regclass),
    "code" varchar(255) NOT NULL,
    "roundNum" int4 NOT NULL,
    "createdBy" int4 NOT NULL,
    "winnerId" int4,
    "status" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "Rooms_createdBy_fkey" FOREIGN KEY ("createdBy") REFERENCES "public"."Users"("id"),
    CONSTRAINT "Rooms_winnerId_fkey" FOREIGN KEY ("winnerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."SequelizeMeta" (
    "name" varchar(255) NOT NULL,
    PRIMARY KEY ("name")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "Users_id_seq";

-- Table Definition
CREATE TABLE "public"."Users" (
    "id" int4 NOT NULL DEFAULT nextval('"Users_id_seq"'::regclass),
    "email" varchar(255) NOT NULL,
    "username" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "password" varchar(255) NOT NULL,
    "role" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(190, 141, 3, 'ROCK', '2022-09-26 12:08:32.14+08', '2022-09-26 12:08:32.14+08', NULL);
INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(191, 141, 5, 'PAPER', '2022-09-26 12:09:20.97+08', '2022-09-26 12:09:20.97+08', NULL);
INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(192, 142, 5, 'PAPER', '2022-09-26 12:12:32.919+08', '2022-09-26 12:12:32.919+08', NULL);
INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(193, 142, 3, 'ROCK', '2022-09-26 12:13:15.978+08', '2022-09-26 12:13:15.978+08', NULL),
(194, 143, 3, 'ROCK', '2022-09-26 12:13:39.637+08', '2022-09-26 12:13:39.637+08', NULL),
(195, 143, 5, 'ROCK', '2022-09-26 12:14:04.379+08', '2022-09-26 12:14:04.379+08', NULL),
(196, 144, 3, 'ROCK', '2022-09-26 13:22:41.02+08', '2022-09-26 13:22:41.02+08', NULL),
(197, 144, 5, 'ROCK', '2022-09-26 13:23:41.246+08', '2022-09-26 13:23:41.246+08', NULL),
(198, 145, 5, 'ROCK', '2022-09-26 13:23:42.916+08', '2022-09-26 13:23:42.916+08', NULL),
(199, 145, 3, 'ROCK', '2022-09-26 13:24:01.612+08', '2022-09-26 13:24:01.612+08', NULL),
(200, 146, 3, 'ROCK', '2022-09-26 13:24:03.38+08', '2022-09-26 13:24:03.38+08', NULL),
(201, 146, 5, 'ROCK', '2022-09-26 13:24:24.222+08', '2022-09-26 13:24:24.222+08', NULL);

INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(141, 9, 5, 'INACTIVE', '2022-09-26 12:08:32.052+08', '2022-09-26 12:09:20.989+08', NULL);
INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(142, 9, 5, 'INACTIVE', '2022-09-26 12:12:32.832+08', '2022-09-26 12:13:16.005+08', NULL);
INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(143, 9, NULL, 'INACTIVE', '2022-09-26 12:13:39.599+08', '2022-09-26 12:14:04.394+08', NULL);
INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(144, 10, NULL, 'INACTIVE', '2022-09-26 13:22:40.998+08', '2022-09-26 13:23:41.253+08', NULL),
(145, 10, NULL, 'INACTIVE', '2022-09-26 13:23:42.905+08', '2022-09-26 13:24:01.619+08', NULL),
(146, 10, NULL, 'INACTIVE', '2022-09-26 13:24:03.369+08', '2022-09-26 13:24:24.233+08', NULL);

INSERT INTO "public"."RoomPlayers" ("id", "roomId", "playerId", "createdAt", "updatedAt", "deletedAt") VALUES
(4, 9, 3, '2022-09-25 15:47:32.373+08', '2022-09-25 15:47:32.373+08', NULL);
INSERT INTO "public"."RoomPlayers" ("id", "roomId", "playerId", "createdAt", "updatedAt", "deletedAt") VALUES
(5, 9, 5, '2022-09-25 15:48:00.712+08', '2022-09-25 15:48:00.712+08', NULL);
INSERT INTO "public"."RoomPlayers" ("id", "roomId", "playerId", "createdAt", "updatedAt", "deletedAt") VALUES
(6, 10, 5, '2022-09-26 13:17:45.869+08', '2022-09-26 13:17:45.869+08', NULL);
INSERT INTO "public"."RoomPlayers" ("id", "roomId", "playerId", "createdAt", "updatedAt", "deletedAt") VALUES
(7, 10, 3, '2022-09-26 13:19:21.629+08', '2022-09-26 13:19:21.629+08', NULL);

INSERT INTO "public"."Rooms" ("id", "code", "roundNum", "createdBy", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(9, '810', 3, 5, 5, 'FINISHED', '2022-09-25 15:13:03.829+08', '2022-09-26 12:14:04.429+08', NULL);
INSERT INTO "public"."Rooms" ("id", "code", "roundNum", "createdBy", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(10, '5yYG', 3, 5, NULL, 'FINISHED', '2022-09-26 13:02:57.601+08', '2022-09-26 13:24:24.245+08', NULL);


INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220829125251-create-user.js');
INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220829133949-create-user-game-biodata.js');
INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220829134702-create-user-game-history.js');
INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220830050645-create-user-games.js'),
('20220830051136-create-user-games.js'),
('20220831182426-create-players.js'),
('20220901183134-create-user-game-biodata.js'),
('20220902141559-create-user-game-history.js'),
('20220902183216-create-user-game-biodata.js'),
('20220905001132-create-user-game-history.js'),
('20220905070023-create-history.js'),
('20220920055930-create-history.js'),
('20220920061520-create-user-game-biodata.js'),
('20220920062127-create-history.js'),
('20220921000933-create-users.js'),
('20220921001719-create-rooms.js'),
('20220921003301-create-room-players.js'),
('20220921003822-create-game-rounds.js'),
('20220921004342-create-game-round-histories.js');

INSERT INTO "public"."Users" ("id", "email", "username", "name", "password", "role", "createdAt", "updatedAt", "deletedAt") VALUES
(1, 'duplicate@gmail.com', 'binaracademy', 'Binar Player', '$2b$10$5ooQ3LHiPmHBppGCdnPqFuKDFiEXUe9UPDw/RF/5r6D.UZawHs5BO', 'player', '2022-09-21 21:46:02.724+08', '2022-09-21 21:46:02.724+08', NULL);
INSERT INTO "public"."Users" ("id", "email", "username", "name", "password", "role", "createdAt", "updatedAt", "deletedAt") VALUES
(3, 'duplicate1@gmail.com', 'binaracademy1', 'Binar Player1', '$2b$10$jk9wSaNIQBucc338n9idh.fRNacIXqKlqfW6fkau8La/HR.Tz1UwG', 'player', '2022-09-21 21:50:35.644+08', '2022-09-21 21:50:35.644+08', NULL);
INSERT INTO "public"."Users" ("id", "email", "username", "name", "password", "role", "createdAt", "updatedAt", "deletedAt") VALUES
(5, 'duplicate2@gmail.com', 'binaracademy2', 'Binar Player2', '$2b$10$FtLF552F7kqO8lOOrxuqcu2rU6ixBteIyFOlYrWSZ/qbdcXIDMcru', 'player', '2022-09-21 21:57:39.338+08', '2022-09-21 21:57:39.338+08', NULL);
INSERT INTO "public"."Users" ("id", "email", "username", "name", "password", "role", "createdAt", "updatedAt", "deletedAt") VALUES
(6, 'duplicate3@gmail.com', 'binaracademy3', 'Binar Player3', '$2b$10$cIJSLMHbAx7Dk84VMRYwPu2yCmWrGTI2Mnbo7RWFYqkifKtZLTRqq', 'player', '2022-09-26 13:25:25.668+08', '2022-09-26 13:25:25.668+08', NULL);
