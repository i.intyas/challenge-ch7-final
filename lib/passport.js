
const passport = require('passport')
const { Strategy : JwtStrategy, ExtractJwt } = require('passport-jwt')


passport.use(
    new JwtStrategy (
    {
        jwtFromRequest: ExtractJwt.fromHeader("authorization"),
        secretOrKey: "secret",
    }, 
    (payload, done) => {
        console.log("payload jwt",payload);
        done(null,{
            id : payload.id,
            username : payload.username
        }) 
    }));
        
        
    // Kita exports karena akan kita gunakan sebagai middleware    
    module.exports = passport;

        